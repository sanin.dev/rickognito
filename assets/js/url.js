document.addEventListener("DOMContentLoaded", function () {
    const url = new URL(window.location.href);
    let input = document.getElementById('urlinput');
    let output = document.getElementById('urloutput');
    let protocol = new RegExp('^[A-Za-z]+://');

    function oninput(e) {
        let intext = e.currentTarget.value.replace(protocol, '');
        let components = intext.split('/').reverse();
        if (components.length && !components[0]) {
            components.push(components.shift());
        }
        output.value = `${url.protocol}//${url.hostname}/${components.join('/')}`;
    }

    input.addEventListener('change', oninput);
    input.addEventListener('input', oninput);
});