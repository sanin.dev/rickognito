const fs = require('fs');
const path = require('path');
const express = require('express');
const exuseragent = require('express-useragent');


class Web {
    constructor() {
        const port = process.env.PORT || 8080;
        const app = express();
        app.set('trust proxy', 1);
        app.use(exuseragent.express());
        app.use('/assets/', express.static('assets'));

        app.get('/', (req, res) => {
            res.sendFile(path.join(__dirname, 'views', 'index.html'));
        });

        app.get('/healthcheck', (req, res) => {
            res.send('Healthy');
        });

        app.get('/*', (req, res) => {
            if (req.useragent.isBot) {
                let components = req.url.substring(1).split('/').reverse();
                if (components.length && !components[0]) {
                    components.push(components.shift());
                }
                res.redirect(`https://${components.join('/')}`);
            }
            else {
                res.redirect('https://www.youtube.com/watch?v=dQw4w9WgXcQ');
            }
        });

        this._server = app.listen(port, () => console.log(`rickognito running on port ${port}!`));
    }

    close() {
        if (this._server) {
            this._server.close();
        }
    }
}

module.exports = Web;
