FROM oven/bun:alpine AS install

WORKDIR /usr/src/app

COPY package*.json ./
COPY bun.lockb ./

ENV NPM_CONFIG_LOGLEVEL warn
RUN bun install --production --no-progress

COPY . .

FROM oven/bun:alpine

HEALTHCHECK  --timeout=3s \
  CMD curl --fail http://localhost:8080/healthcheck || exit 1
RUN apk add --no-cache curl

WORKDIR /usr/src/app

COPY --from=install /usr/src/app /usr/src/app/

USER bun

CMD [ "bun", "index.js" ]

EXPOSE 8080