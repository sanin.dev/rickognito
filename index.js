const Web = require('./Web.js');

const web = new Web();
process.on('SIGTERM', () => {
    web.close();
});
