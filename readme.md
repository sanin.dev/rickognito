# Rickognito
![Rickognito](/assets/images/rick.gif "Rickognito")

A web server that directs link preview bots to a webpage of your choosing while directing humans to [Never Gonna Give You Up](https://www.youtube.com/watch?v=qdtOhtjWaOE).

Try it out at [aggregationnews.com](https://aggregationnews.com/).